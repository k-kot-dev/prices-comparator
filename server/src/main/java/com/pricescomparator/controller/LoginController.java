package com.pricescomparator.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @GetMapping("/login")
    public String loginPage() {
        return "User successfully logged in";
    }

    @GetMapping("/")
    public String homePage() {
        return "Welcome to Home Page - no need to be logged in";
    }
}
