package com.pricescomparator.controller;


import com.pricescomparator.configuration.ResourceNotFoundException;
import com.pricescomparator.datamodel.CarModel;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.repository.CarModelsRepository;
import com.pricescomparator.repository.MakesRepository;
import com.pricescomparator.service.PaginationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class CarModelController {

    @Autowired
    private CarModelsRepository carModelsRepository;

    @Autowired
    private PaginationService paginationService;

    @Autowired
    private MakesRepository makesRepository;

    //Advanced paging - f.ex. http://localhost:8080/carmodels?pageSize=200&pageNo=1&sortBy=id After ? Query parameters
    @GetMapping("/carmodels")
    public ResponseEntity<List<CarModel>> findAllCarModelsByPage(
            @RequestParam(defaultValue = "1") Integer pageNo,
            @RequestParam(defaultValue = "100") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) {
        List<CarModel> carmodels = paginationService.getAllCarModels(pageNo, pageSize, sortBy);
        HttpHeaders responseHeaders = new HttpHeaders();     //HttpHeaders.CACHE_CONTROL - used previously
        responseHeaders.add("number-of-all-carmodels", String.valueOf(carModelsRepository.count())); //Axios is using this
        return new ResponseEntity<List<CarModel>>(carmodels, responseHeaders, HttpStatus.OK);
    }

    @GetMapping("/carmodels/{carModelId}") //f.ex. 21000 - Path parameter
    private CarModel getCarModelById(@PathVariable int carModelId) throws Exception {
        if (!carModelsRepository.existsById(carModelId)) {
            throw new ResourceNotFoundException(carModelId);
        }
        return carModelsRepository.findCarModelById(carModelId);
    }

    //I cannot simply post "/carmodels" because we need automatically set Make for CarModel - Hibernate requires this
    @PostMapping("/makes/{makeId}/carmodels")
    private CarModel saveOrCreateCarModel(@PathVariable int makeId,
                                          @Valid @RequestBody CarModel carModelRequest) throws Exception {
        if (!makesRepository.existsById(makeId)) {
            throw new ResourceNotFoundException(makeId);
        }
        Make make = makesRepository.findMakeById(makeId);
        carModelRequest.setMake(make);
        return carModelsRepository.save(carModelRequest);
    }

    //Without requirement to Make - you can f.ex. change name of CarModel
    @PutMapping("carmodels/{carModelId}")
    private CarModel updateCarModel(@PathVariable int carModelId,
                                    @Valid @RequestBody CarModel carModelRequest) throws Exception {
        if (!carModelsRepository.existsById(carModelId)) {
            throw new ResourceNotFoundException(carModelId);
        }
        CarModel carModel = carModelsRepository.findCarModelById(carModelId);
        carModel.setName(carModelRequest.getName()); //Only name can be changed
        return carModelsRepository.save(carModel);
    }

    //Without requirement to Make - you can f.ex. delete CarModel
    @DeleteMapping("carmodels/{carModelId}")
    private ResponseEntity<?> deleteCarModel(@PathVariable int carModelId) throws Exception {
        if (!carModelsRepository.existsById(carModelId)) {
            throw new ResourceNotFoundException(carModelId);
        }
        CarModel carModel = carModelsRepository.findCarModelById(carModelId);
        carModelsRepository.delete(carModel);
        return ResponseEntity.ok().build();
    }


    //Setup of relation between Make and Carmodel - adding/deleting connections
    
    @GetMapping("/carmodels/{carModelId}/make") //f.ex. 21000 - Path parameter
    private Make getMakeByCarModelId(@PathVariable int carModelId) throws Exception {
        if (!carModelsRepository.existsById(carModelId)) {
            throw new ResourceNotFoundException(carModelId);
        }
        return makesRepository.findMakeByCarModelsId(carModelId);
    }

    @GetMapping("makes/{makeId}/carmodels") //f.ex. 21000 - Path parameter
    private List<CarModel> getCarModelsByMakeId(@PathVariable int makeId) throws Exception {
        if (!makesRepository.existsById(makeId)) {
            throw new ResourceNotFoundException(makeId);
        }
        return carModelsRepository.findCarModelsByMakeId(makeId);
    }

    //Relation between Make and CarModel is created during Post - after Post you can only change make_id for specified CarModel
    //You cannot create new relation or delete relation - you can only change it
    //That is why we can only use Put - change
    @PutMapping("/makes/{makeId}/carmodels/{carModelId}")
    private CarModel updateCarModelsByMakeId(@PathVariable int makeId, @PathVariable int carModelId) throws Exception {
        if (!makesRepository.existsById(makeId)) {
            throw new ResourceNotFoundException(makeId);
        }
        if (!carModelsRepository.existsById(carModelId)) {
            throw new ResourceNotFoundException(carModelId);
        }
        Make make = makesRepository.findMakeById(makeId);
        List<CarModel> carModelList = carModelsRepository.findCarModelsByMakeId(makeId);
        CarModel carModel = carModelsRepository.findCarModelById(carModelId);
        if (!carModelList.contains(carModel)) {
            carModel.setMake(make);
            return carModelsRepository.save(carModel);
            //You don't need to set for Make new List of Carmodels - Hibernate makes it change during saving CarModel
            //You just don't need to set it in both directions - it can be done by CarModel
            // carModelList.add(carModel);
            //make.setCarModels(carModelList);
            //makesRepository.save(make);
        } else {
            System.out.println("There is CarModel exsiting in relation to Make");
            return null;
        }
    }
}




