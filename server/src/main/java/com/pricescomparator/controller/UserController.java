package com.pricescomparator.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pricescomparator.datamodel.User;
import com.pricescomparator.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/users")
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @PostMapping("/users")
    private ResponseEntity createOrSaveUser(@RequestBody User userRequest){
        //Checking if user exists
        if(userRepository.findByUserName(userRequest.getUserName()) == null){
            User user = new User();
            user.setUserName(userRequest.getUserName());
            user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
            user.setActive(1);
            user.setRoles("USER");
            user.setPermissions("");
            return new ResponseEntity<>(userRepository.save(user),HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("This user exists",HttpStatus.FOUND);
        }
    }

    @PutMapping("/users")
    private ResponseEntity changePassword(@RequestBody ObjectNode objectNode){
        String userName = objectNode.get("userName").asText();
        String oldPassword = objectNode.get("oldPassword").asText();
        String newPassword = objectNode.get("newPassword").asText();

        if(userName != "" && oldPassword != "" && newPassword != ""){
            if(userRepository.findByUserName(userName) != null){
                User user = userRepository.findByUserName(userName);
                if(passwordEncoder.matches(oldPassword,user.getPassword())){
                    user.setPassword(passwordEncoder.encode(newPassword));
                    return new ResponseEntity<>(userRepository.save(user), HttpStatus.CREATED);//201
                } else {
                    return new ResponseEntity<>("Invalid old password",HttpStatus.NOT_FOUND); //404
                }
            }
            return new ResponseEntity<>("User does not exist",HttpStatus.BAD_REQUEST); //400
        }
        return new ResponseEntity<>("Invalid input data!", HttpStatus.INTERNAL_SERVER_ERROR);//500
    }
}
