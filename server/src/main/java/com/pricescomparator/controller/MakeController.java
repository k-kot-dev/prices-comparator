package com.pricescomparator.controller;

import com.pricescomparator.configuration.ResourceNotFoundException;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.datamodel.Manufacturer;
import com.pricescomparator.repository.MakesRepository;
import com.pricescomparator.repository.ManufacturersRepository;
import com.pricescomparator.service.PaginationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MakeController {

    @Autowired
    private MakesRepository makesRepository;

    @Autowired
    private PaginationService paginationService;

    //You can switch 2 methods between controllers and disable @Autowired here
    @Autowired
    private ManufacturersRepository manufacturersRepository;

    //Advanced paging - f.ex. http://localhost:8080/makes?pageSize=200&pageNo=1&sortBy=id
    //?/pageSize={pageSize}&pageNo={pageNo}&sortBy={sortBy}"
    @GetMapping("/makes")
    public ResponseEntity<List<Make>> findAllMakesByPage(
            @RequestParam(defaultValue = "1") Integer pageNo,
            @RequestParam(defaultValue = "100") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) {
        List<Make> makes = paginationService.getAllMakes(pageNo, pageSize, sortBy);
        HttpHeaders responseHeaders = new HttpHeaders();     //HttpHeaders.CACHE_CONTROL - used previously
        responseHeaders.add("number-of-all-makes", String.valueOf(makesRepository.count())); //Axios is using this
        return new ResponseEntity<List<Make>>(makes, responseHeaders, HttpStatus.OK);
    }

    @GetMapping("/makes/{makeId}") //f.ex.6473
    private Make getMake(@PathVariable int makeId) throws Exception {
        if (!makesRepository.existsById(makeId)) {
            throw new ResourceNotFoundException(makeId);
        }
        return makesRepository.findMakeById(makeId);
    }

    @PostMapping("/makes")
    private ResponseEntity createOrSaveMake(@Valid @RequestBody Make make) {
        try {
            return new ResponseEntity<>(makesRepository.save(make), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("/makes/{makeId}") //f.ex.6473
    private Make updateMake(@PathVariable int makeId, @Valid @RequestBody Make makeRequest) throws Exception {
        if (!makesRepository.existsById(makeId)) {
            throw new ResourceNotFoundException(makeId);
        }
        Make make = makesRepository.findMakeById(makeId);
        make.setName(makeRequest.getName()); //Only name can be changed
        return makesRepository.save(make);
    }

    @DeleteMapping("/makes/{makeId}")
    private ResponseEntity<?> deleteMake(@PathVariable int makeId) throws Exception {
        if (!makesRepository.existsById(makeId)) {
            throw new ResourceNotFoundException(makeId);
        }
        Make make = makesRepository.findMakeById(makeId);
        makesRepository.delete(make);
        return ResponseEntity.ok().build();
    }

    //Setup of relation between Make and Manufacturer - adding/deleting connections

    @GetMapping("/makes/{makesId}/manufacturers") //f.ex.446
    private List<Manufacturer> getManufacturersByMakesId(@PathVariable int makesId) throws Exception {
        if (!makesRepository.existsById(makesId)) {
            throw new ResourceNotFoundException(makesId);
        }
        return manufacturersRepository.findManufacturersByMakesId(makesId);
    }

    //This is working when you put JoinColums inside Make Class
    @PostMapping("/makes/{makesId}/manufacturers/{manufacturersId}")
    private void createManufacturersByMakesId(@PathVariable int makesId, @PathVariable int manufacturersId) throws Exception {
        if (!makesRepository.existsById(makesId)) {
            throw new ResourceNotFoundException(makesId);
        }
        if (!manufacturersRepository.existsById(manufacturersId)) {
            throw new ResourceNotFoundException(manufacturersId);
        }
        //We find make, we want to add make connection
        Make make = makesRepository.findMakeById(makesId);
        //We take actual list of manufacturers for this make, not to remove before overwriting
        List<Manufacturer> manufacturerList = manufacturersRepository.findManufacturersByMakesId(makesId);
        Manufacturer manufacturer = manufacturersRepository.findManufacturerById(manufacturersId);
        if (!manufacturerList.contains(manufacturer)) {
            manufacturerList.add(manufacturer);
            make.setManufacturers(manufacturerList);
            makesRepository.save(make);
        } else {
            System.out.println("There is Manfufacturer exsiting in relation to Make");
        }

    }

    @DeleteMapping("/makes/{makesId}/manufacturers/{manufacturersId}")
    private ResponseEntity<?> deleteManufacturersByMakesId(@PathVariable int makesId, @PathVariable int manufacturersId) throws Exception {
        if (!makesRepository.existsById(makesId)) {
            throw new ResourceNotFoundException(makesId);
        }
        if (!manufacturersRepository.existsById(manufacturersId)) {
            throw new ResourceNotFoundException(manufacturersId);
        }
        Make make = makesRepository.findMakeById(makesId);
        List<Manufacturer> manufacturerList = manufacturersRepository.findManufacturersByMakesId(makesId);
        Manufacturer manufacturer = manufacturersRepository.findManufacturerById(manufacturersId);
        if (manufacturerList.contains(manufacturer)) {
            manufacturerList.remove(manufacturer);
            make.setManufacturers(manufacturerList);
            makesRepository.save(make);
            return ResponseEntity.ok().build();
        } else {
            System.out.println("There is Manfufacturer not exsiting in relation to Make");
            return ResponseEntity.badRequest().build();
        }
    }

}
