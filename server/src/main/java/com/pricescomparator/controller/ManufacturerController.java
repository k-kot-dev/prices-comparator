package com.pricescomparator.controller;

import com.pricescomparator.configuration.ResourceNotFoundException;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.datamodel.Manufacturer;
import com.pricescomparator.repository.MakesRepository;
import com.pricescomparator.repository.ManufacturersRepository;
import com.pricescomparator.service.PaginationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class ManufacturerController {

    @Autowired
    private ManufacturersRepository manufacturersRepository;

    @Autowired
    private MakesRepository makesRepository;

    @Autowired
    private PaginationService paginationService;

    //Advanced paging - f.ex. http://localhost:8080/manufacturers?pageSize=200&pageNo=1&sortBy=id
    @GetMapping("/manufacturers")
    public ResponseEntity<List<Manufacturer>> findAllManufacturersByPage(
            @RequestParam(defaultValue = "1") Integer pageNo,
            @RequestParam(defaultValue = "100") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) {
        List<Manufacturer> manufacturers = paginationService.getAllManufacturers(pageNo, pageSize, sortBy);
        HttpHeaders responseHeaders = new HttpHeaders();     //HttpHeaders.CACHE_CONTROL - used previously
        responseHeaders.add("number-of-all-manufacturers", String.valueOf(manufacturersRepository.count())); //Axios is using this
        return new ResponseEntity<List<Manufacturer>>(manufacturers, responseHeaders, HttpStatus.OK);
    }

    @GetMapping("/manufacturers/{manufacturerId}")
    private Manufacturer getManufacturerById(@PathVariable int manufacturerId) throws Exception {
        if (!manufacturersRepository.existsById(manufacturerId)) {
            throw new ResourceNotFoundException(manufacturerId);
        }
        return manufacturersRepository.findManufacturerById(manufacturerId);
    }

    @PostMapping("/manufacturers")
    private ResponseEntity createOrSaveManufacturer(@Valid @RequestBody Manufacturer manufacturer) {
        try {
            return new ResponseEntity<>(manufacturersRepository.save(manufacturer), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("/manufacturers/{manufacturerId}") //f.ex.6473
    private Manufacturer updateManufacturer(@PathVariable int manufacturerId,
                                            @Valid @RequestBody Manufacturer manufacturerRequest) throws Exception {
        if (!manufacturersRepository.existsById(manufacturerId)) {
            throw new ResourceNotFoundException(manufacturerId);
        }
        Manufacturer manufacturer = manufacturersRepository.findManufacturerById(manufacturerId); //We have manufacturer
        manufacturer.setName(manufacturerRequest.getName());
        manufacturer.setCommonName(manufacturerRequest.getCommonName());
        manufacturer.setCountry(manufacturerRequest.getCountry());
        manufacturer.setManufacturerDetails(manufacturerRequest.getManufacturerDetails());
        return manufacturersRepository.save(manufacturer);
    }

    @DeleteMapping("/manufacturers/{manufacturerId}")
    private ResponseEntity<?> deleteManufacturer(@PathVariable int manufacturerId) throws Exception {
        if (!manufacturersRepository.existsById(manufacturerId)) {
            throw new ResourceNotFoundException(manufacturerId);
        }
        Manufacturer manufacturer = manufacturersRepository.findManufacturerById(manufacturerId);
        manufacturersRepository.delete(manufacturer);
        return ResponseEntity.ok().build();
    }


    //Setup of relation between Manufacturer and Make - adding/deleting connections

    @GetMapping("/manufacturers/{manufacturersId}/makes") // f.ex.962
    private List<Make> getMakesByManufacturersId(@PathVariable int manufacturersId) throws Exception {
        if (!manufacturersRepository.existsById(manufacturersId)) {
            throw new ResourceNotFoundException(manufacturersId);
        }
        return makesRepository.findMakesByManufacturersId(manufacturersId);
    }

    @PostMapping("/manufacturers/{manufacturersId}/makes/{makesId}") // f.ex.962
    private void createMakesByManufacturersId(@PathVariable int manufacturersId, @PathVariable int makesId) throws Exception {
        if (!manufacturersRepository.existsById(manufacturersId)) {
            throw new ResourceNotFoundException(manufacturersId);
        }
        if (!makesRepository.existsById(makesId)) {
            throw new ResourceNotFoundException(makesId);
        }
        //We find manufacturer, we want to add make connection
        Manufacturer manufacturer = manufacturersRepository.findManufacturerById(manufacturersId);
        //We take actual list of makes for this manufacturer, not to remove before overwriting
        List<Make> makeList = makesRepository.findMakesByManufacturersId(manufacturersId);
        Make make = makesRepository.findMakeById(makesId);
        if (!makeList.contains(make)) {
            makeList.add(make);
            manufacturer.setMakes(makeList);
            manufacturersRepository.save(manufacturer);
        } else {
            System.out.println("There is Make exsiting in relation to manufacturer");
        }

    }

    @DeleteMapping("/manufacturers/{manufacturersId}/makes/{makesId}") // f.ex.962
    private ResponseEntity<?> deleteMakesByManufacturersId(@PathVariable int manufacturersId, @PathVariable int makesId) throws Exception {
        if (!manufacturersRepository.existsById(manufacturersId)) {
            throw new ResourceNotFoundException(manufacturersId);
        }
        if (!makesRepository.existsById(makesId)) {
            throw new ResourceNotFoundException(makesId);
        }
        //We find manufacturer, we want to add make connection
        Manufacturer manufacturer = manufacturersRepository.findManufacturerById(manufacturersId);
        //We take actual list of makes for this manufacturer, not to remove before overwriting
        List<Make> makeList = makesRepository.findMakesByManufacturersId(manufacturersId);
        Make make = makesRepository.findMakeById(makesId);
        if (makeList.contains(make)) {
            makeList.remove(make);
            manufacturer.setMakes(makeList);
            manufacturersRepository.save(manufacturer);
            return ResponseEntity.ok().build();
        } else {
            System.out.println("There is Make not exsiting in relation to manufacturer");
            return ResponseEntity.badRequest().build();
        }
    }
}
