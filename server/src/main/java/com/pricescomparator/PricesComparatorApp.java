package com.pricescomparator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class PricesComparatorApp {

    //Here he finds all Entities, Repositories and generates Java code
    public static void main(String[] args) {
        SpringApplication.run(PricesComparatorApp.class);

    }
}
