package com.pricescomparator.datamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;

@Entity
@Table(name = "carmodels")
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    @SerializedName("Model_ID") //only for deserialization
    private int id;

    @Column(name = "name")
    @SerializedName("Model_Name") //only for deserialization
    private String name;

    //Hibernate creates this table, whose name is "make_id", and it connects the ID from class Make,
    //Because relation is always between primary key (@Id)
    @ManyToOne
    @JsonIgnore //Used by Jackson databinder not to recurse
    @JoinColumn(name = "make_id", nullable = false)
    private Make make;

    //Hibernate requires public constructor without initialization - getters/setters has to be used
    public CarModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Make getMake() {
        return make;
    }

    public void setMake(Make make) {
        this.make = make;
    }
}
