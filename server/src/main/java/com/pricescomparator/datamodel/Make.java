package com.pricescomparator.datamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "makes")
public class Make {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) //17.04
    @Column(name = "id", updatable = false, nullable = false)
    @SerializedName("Make_ID")
    private int id;

    @Column(name = "name")
    @SerializedName("Make_Name")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "make")
    private List<CarModel> carModels;

    //This setup allows you to add/delete manufacturers for make
    @JsonIgnore //Ignores relation during data binding for jackson (API response)
    @ManyToMany
    @JoinTable(name = "manufacturers_makes", //Name of new table, because @ManyToMany requires new table
            joinColumns = {@JoinColumn(name = "make_id")}, //Name of column from Manufacturer_ID
            inverseJoinColumns = {@JoinColumn(name = "manufacturer_id")}) //Name of column from Make_ID
    private List<Manufacturer> manufacturers;

//    This setup not allows you to add/delete manufacturers for make
//    @JsonIgnore
//    @ManyToMany(mappedBy = "makes")
//    private List<Manufacturer> manufacturers;

    //Hibernate requires public constructor without initialization - getters/setters has to be used
    public Make() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //    @Transactional
    public List<CarModel> getCarModels() {
        return carModels;
    }

    public void setCarModels(List<CarModel> carModels) {
        this.carModels = carModels;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

}

