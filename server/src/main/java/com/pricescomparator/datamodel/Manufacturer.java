package com.pricescomparator.datamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "manufacturers")
public class Manufacturer {

    @Id
    @Column(name = "id", updatable = false, nullable = false) //Cannot be updated and be null
    @GeneratedValue(strategy = GenerationType.AUTO)
    @SerializedName("Mfr_ID")
    private int id;

    @Column(name = "name")
    @SerializedName("Mfr_Name")
    private String name;

    @Column(name = "commonname")
    @SerializedName("Mfr_CommonName")
    private String commonName;

    @Column(name = "country")
    @SerializedName("Country")
    private String country;

    @Embedded //It means, that data from ManufacturerDetails will be put in this table-Manufacturer
    //There cannot be List<ManufacturerDetails>, because Hibernate requires Object, not List
    private ManufacturerDetails manufacturerDetails;

    //@ManyToMany //unidirectional -works only, when it's also written @ManyToMany in Make
    //JoinTable is made here, because we save Manufacturer Object during parsing, not Make-Make is only mappedBy

    //This setup not allows you to add/delete makes for manufacturer
//    @JsonIgnore
//    @ManyToMany (mappedBy = "manufacturers")
//    private List<Make> makes;

    //This setup allows you to add/delete makes for manufacturer
    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "manufacturers_makes", //Name of new table, because @ManyToMany requires new table
            joinColumns = {@JoinColumn(name = "manufacturer_id")}, //Name of column from Manufacturer_ID
            inverseJoinColumns = {@JoinColumn(name = "make_id")}) //Name of column from Make_ID
    private List<Make> makes;

    public Manufacturer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ManufacturerDetails getManufacturerDetails() {
        return manufacturerDetails;
    }

    public void setManufacturerDetails(ManufacturerDetails manufacturerDetails) {
        this.manufacturerDetails = manufacturerDetails;
    }

    public List<Make> getMakes() {
        return makes;
    }

    public void setMakes(List<Make> makes) {
        this.makes = makes;
    }
}
