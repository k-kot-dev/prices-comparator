package com.pricescomparator.datamodel;

import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Embeddable;

//When class is Embeddable, you don't use Column names and "Entity" annotation
@Embeddable //Data from here will be put into Manufacturer Class
public class ManufacturerDetails {

    @Column (name = "address") //(updatable = false) - use this when you parse new Manufacturers from REST API- not to make null
    @SerializedName("Address")
    private String address;

    @Column(name = "city") //(updatable = false)
    @SerializedName("City")
    private String city;

    @Column(name = "contactemail", updatable = false)
    @SerializedName("ContactEmail")
    private String contactEmail;

    @Column(name = "othermanufacturerdetails") //(updatable = false)
    @SerializedName("OtherManufacturerDetails")
    private String otherManufacturerDetails;

    public ManufacturerDetails() {}

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getOtherManufacturerDetails() {
        return otherManufacturerDetails;
    }

    public void setOtherManufacturerDetails(String otherManufacturerDetails) {
        this.otherManufacturerDetails = otherManufacturerDetails;
    }
}
