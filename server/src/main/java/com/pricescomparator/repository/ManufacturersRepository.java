package com.pricescomparator.repository;

import com.pricescomparator.datamodel.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ManufacturersRepository extends JpaRepository<Manufacturer, Integer> {

    List<Manufacturer> findAll();
    Manufacturer findManufacturerById(int manufacturerId);
    List<Manufacturer> findManufacturersByMakesId(int makesId);

}
