package com.pricescomparator.repository;

import com.pricescomparator.datamodel.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarModelsRepository extends JpaRepository<CarModel, Integer> {
    CarModel findCarModelById(int id);
    List<CarModel> findAll();
    List<CarModel> findCarModelsByMakeId(int makeId);

}
