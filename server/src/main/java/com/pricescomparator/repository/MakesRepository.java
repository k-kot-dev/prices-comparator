package com.pricescomparator.repository;

import com.pricescomparator.datamodel.Make;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MakesRepository extends JpaRepository<Make, Integer> { //PagingAndSortingRepository<Make, Integer> - not necessary here

    List<Make> findAll();
    Make findMakeById(int id); //now let's go to controller
    List<Make> findMakesByManufacturersId(int manufacturerId);
    Make findMakeByCarModelsId(int carModelId);
    //Naming is crucial - it has to be ManufacturersId, not ManufacturerId, because Spring
    //finds field Manufacturers , and then looks for Id
    //but it can be Make or Makes - it doesn't matter

}
