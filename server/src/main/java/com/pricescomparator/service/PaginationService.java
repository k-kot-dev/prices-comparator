package com.pricescomparator.service;

import com.pricescomparator.datamodel.CarModel;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.datamodel.Manufacturer;
import com.pricescomparator.repository.CarModelsRepository;
import com.pricescomparator.repository.MakesRepository;
import com.pricescomparator.repository.ManufacturersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaginationService {

    @Autowired
    MakesRepository makesRepository;

    @Autowired
    CarModelsRepository carModelsRepository;

    @Autowired
    ManufacturersRepository manufacturersRepository;

    public List<Make> getAllMakes(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<Make> pagedResult = makesRepository.findAll(paging);
        if(pagedResult.hasContent()){
            return pagedResult.getContent();
        } else { return new ArrayList<Make>(); }
    }

    public List<Manufacturer> getAllManufacturers(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<Manufacturer> pagedResult = manufacturersRepository.findAll(paging);
        if(pagedResult.hasContent()){
            return pagedResult.getContent();
        } else { return new ArrayList<Manufacturer>(); }
    }

    public List<CarModel> getAllCarModels(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<CarModel> pagedResult = carModelsRepository.findAll(paging);
        if(pagedResult.hasContent()){
            return pagedResult.getContent();
        } else { return new ArrayList<CarModel>(); }
    }

//    public <T> List<T> getAll(Integer pageNo, Integer pageSize, String sortBy) {
//        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
//
//        Page<T> pagedResult = (Page<T>) makesRepository.findAll(paging);
//
//        if(pagedResult.hasContent()){
//            return pagedResult.getContent();
//        } else {
//            return new ArrayList<T>();
//        }
//    }

}
