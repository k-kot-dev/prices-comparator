package com.pricescomparator.service;

import com.pricescomparator.datamodel.User;
import com.pricescomparator.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DatabaseInit implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        //Delete old users
        userRepository.deleteAll();

        //Create users
        User kacper = new User("user",passwordEncoder.encode("pass"),"USER","");
        User admin = new User("admin",passwordEncoder.encode("admin"),"ADMIN","");

        List<User> users = Arrays.asList(kacper,admin);

        //Save to DB
        userRepository.saveAll(users);

    }
}
