package com.pricescomparator;

import com.pricescomparator.datamodel.CarModel;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.datamodel.Manufacturer;

import java.util.List;

public interface VehicleAPI {
    List<Make> getAllMakes();
    List<CarModel> getModelByMake(Make make);
    List<CarModel> getAllModels();
    List<Manufacturer> getAllManufacturers();
    List<Make> getMakesForManufacturer(Manufacturer manufacturer);
    List<Make> getAllMakesFromManufacturer();

    class Factory {
        public static VehicleAPI create() {
            return new OfflineVehicleAPI();
            //return new OnlineVehicleAPI();
        }
    }

}
