package com.pricescomparator;

import com.pricescomparator.datamodel.CarModel;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.datamodel.Manufacturer;
import com.pricescomparator.datasources.Database;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class OfflineVehicleAPI implements VehicleAPI{

    @Override
    public List<Manufacturer> getAllManufacturers() {
        Session session = Database.getSession();
        session.beginTransaction();
        List<Manufacturer> manufacturers = session.createQuery("SELECT man FROM Manufacturer man", Manufacturer.class).getResultList();
        session.getTransaction().commit();
        return manufacturers;
    }

    @Override
    public List<Make> getMakesForManufacturer(Manufacturer manufacturer) {
        Session session = Database.getSession();
        session.beginTransaction();
        List <Make> makesForManufacturer = session.createQuery(
                "SELECT DISTINCT m FROM Make m " + //Distinct means - without duplication
                "JOIN m.manufacturers manufacturer " + //Make has manufacturers field and this JOIN is called "manufacturer"
                "JOIN manufacturer.makes make_table " + //From this joined table we create another join: makes from manufacturer and this JOIN is called "make_table"
                "WHERE manufacturer.id = :targetManufacturerID", Make.class)
        .setParameter("targetManufacturerID", manufacturer.getId())
                .getResultList();
                session.getTransaction().commit();
        //Validation, if Make is being shown
        System.out.println("Manufacturer ID: " + manufacturer.getId()+ " " +  "Manufacturer Name: " + manufacturer.getName());
        makesForManufacturer.forEach(m -> System.out.println("Make ID: " + m.getId() + " " + "Make Name: " + m.getName()));
//        List<CarModel> carModels = getModelByMake(makesForManufacturer.get(0));
//        carModels.forEach(m -> System.out.println(m.getName()));
        return makesForManufacturer;
    }

    @Override
    public List<Make> getAllMakesFromManufacturer() {
        List<Manufacturer> manufacturers = getAllManufacturers();
        List<Make> result = new ArrayList<>();
        for(Manufacturer manufacturer: manufacturers) {
            result.addAll(getMakesForManufacturer(manufacturer));
        }
        return result;
    }
    @Override
    public List<Make> getAllMakes() {
        // 1.Opening session
        Session session = Database.getSession();
        // 2.Beginning transaction
        session.beginTransaction();
        //m means object of Make. Instead od *, in Hibernate it's used by "m", you can also get property f.ex. "Select m.getID from Make m"
        List <Make> makeList = session.createQuery("SELECT m FROM Make m", Make.class).getResultList();
        // 3.Confirm transaction and commit changes
        session.getTransaction().commit();
        // 4.Create List<Make> and returns it based on query results;
        return makeList;
    }

    @Override
    public List<CarModel> getModelByMake(Make make) {
        Session session = Database.getSession();
        session.beginTransaction();
        List <CarModel> carModelsByMake = session.createQuery("SELECT cm FROM CarModel cm WHERE cm.make.id = :makeId", CarModel.class)
                .setParameter("makeId",make.getId())
                .getResultList();
        session.getTransaction().commit();
        return carModelsByMake;
    }

    @Override
    public List<CarModel> getAllModels() {
        Session session = Database.getSession();
        session.beginTransaction();
        List <CarModel> carModels = session.createQuery("SELECT cm FROM CarModel cm", CarModel.class)
                .getResultList();
        session.getTransaction().commit();
        return carModels;
    }

    public void saveManufacturers(List<Manufacturer> manufacturers) {
        Session session = Database.getSession();
        Transaction transaction = session.beginTransaction();
        for (Manufacturer m : manufacturers) {
            session.saveOrUpdate(m);
        }
        transaction.commit();
    }

    public void saveManufacturerDetails(Manufacturer manufacturer) {
        Session session = Database.getSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(manufacturer);
        transaction.commit();
    }

    public void saveManufacturer(Manufacturer manufacturer) {
        Session session = Database.getSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(manufacturer);
        transaction.commit();
    }

    public void saveMakes(List<Make> makes) {
        Session session = Database.getSession();
        Transaction transaction = session.beginTransaction();
        for (Make m : makes) {
            session.saveOrUpdate(m);
        }
        transaction.commit();
    }

    public void saveCarModels(List<CarModel> carModels) {
        Session session = Database.getSession();
        Transaction transaction = session.beginTransaction();
        for (CarModel cm: carModels) {
            session.saveOrUpdate(cm);
        }
        transaction.commit();
    }

}
