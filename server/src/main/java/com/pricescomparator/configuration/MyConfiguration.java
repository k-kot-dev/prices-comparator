package com.pricescomparator.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableWebMvc
@Configuration
public class MyConfiguration implements WebMvcConfigurer {

    //This configuration is necessary for Axios- that it can overcome CORS(Cross Origin resource sharing) and can use data from Port 8080
    //.exposeHeaders is a list of headers, that Axios can get from API response. When you ues fetch API in JS(in port 8080) you have access
    //to all headers. But from Front-End application Axios does not have access.This list is used to transform f.ex number of Makes(Objects)
    //f.ex ("Cache-Control") etc. or new created
    @Override
    public void addCorsMappings(CorsRegistry registry) {

        registry.addMapping("/**") //It has to be **, because pre-flight does not work without authentication
            .allowedOrigins("http://localhost:8081")
            .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
            .exposedHeaders("number-of-all-makes","number-of-all-carmodels","number-of-all-manufacturers")
            .allowCredentials(true).maxAge(3600);
    }
}