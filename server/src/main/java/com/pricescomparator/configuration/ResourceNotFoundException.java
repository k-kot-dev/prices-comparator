package com.pricescomparator.configuration;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//It is better for Spring to throw Exceptions instead of Catching them
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException  extends Exception {
    public ResourceNotFoundException(int id){
        super("Resource with this id: " + id + " not found");
    }
}


