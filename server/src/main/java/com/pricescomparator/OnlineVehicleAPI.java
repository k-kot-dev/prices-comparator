package com.pricescomparator;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.pricescomparator.datamodel.CarModel;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.datamodel.Manufacturer;
import com.pricescomparator.datamodel.ManufacturerDetails;
import okhttp3.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class OnlineVehicleAPI implements VehicleAPI  {
    private OkHttpClient httpClient = new OkHttpClient();
    private Gson gsonParser = new Gson();
    private int pageStartNumber = 1;
    private int pageEndNumber = 1;

    public OnlineVehicleAPI(){}

    private List<Manufacturer> getManufacturers(int number) {
//        String websiteURL = "https://vpic.nhtsa.dot.gov/api/vehicles/getallmanufacturers?format=json&page=2";
            try (Response response = queryForManufacturers(number)) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                String responseFromWebsite = response.body().string();
                JsonResultForManufacturers manufacturers = gsonParser.fromJson(responseFromWebsite, JsonResultForManufacturers.class);
//                saveManufacturers(manufacturers.results); //saves into Database
                return manufacturers.results;
            } catch (Exception e) {
                System.out.println("Error occured during creating response: " + e.getMessage());
                return null;
        }
    }

    private void fetchManufacturerDetails(Manufacturer manufacturer) {
        ///https://vpic.nhtsa.dot.gov/api/vehicles/getmanufacturerdetails/989?format=json
        int manufacturerID = manufacturer.getId();
        try (Response response = queryForManufacturerDetails(manufacturerID)) {
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response); //throw new - throws Exception
            String responseFromWebsite = response.body().string();
            JsonResultForManufacturerDetails manufacturerDetails = gsonParser.fromJson(responseFromWebsite, JsonResultForManufacturerDetails.class);
            //Get(0) means, that we use first object from an ArrayList (there is always only one object)
            manufacturer.setManufacturerDetails(manufacturerDetails.results.get(0)); //place of Embedded action
//            saveManufacturerDetails(manufacturer); //saves into Database Manufacturer Object
        } catch (Exception e) {
            System.out.println("Error occured during creating response: " + e.getMessage());
        }
    }

    //Injecting Embedded values from ManufacturerDetails
    @Override
    public List<Manufacturer> getAllManufacturers() {
        List<Manufacturer> manufacturers = new ArrayList<>();
        for(int number = pageStartNumber; number <= pageEndNumber; number++){
            manufacturers.addAll(getManufacturers(number));
        }
        for(Manufacturer manufacturer: manufacturers){
            fetchManufacturerDetails(manufacturer);
        }
        return manufacturers;
    }
    @Override
    public List<Make> getMakesForManufacturer(Manufacturer manufacturer) {
        int manufacturerID = manufacturer.getId();
        try (Response response = queryForMake(manufacturerID)) {
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response); //throw new - throws Exception
            String responseFromWebsite = response.body().string();
            JsonResultForMake makes = gsonParser.fromJson(responseFromWebsite, JsonResultForMake.class);

            manufacturer.setMakes(makes.results); //place of @ManyToMany action;
//            saveManufacturer(manufacturer); //saves into Database Manufacturer Object

            //You cannot use it, when it's Unidirectional
            //Additionally, we make changes in Manufacturer Object, we don't want to involve Make Objects in saving
            //We want to leave it as it was (no new columns in "makes") and create connection in another table manufacturers_makes
//            makes.results.forEach(c -> c.setManufacturer(manufacturer));
//            saveMakes(makes.results);
            return makes.results;
        } catch (Exception e) {
            System.out.println("Error occured during creating response: " + e.getMessage());
            return null;
        }
    }

    @Override
    public List<Make> getAllMakesFromManufacturer() {
        List<Manufacturer> manufacturers = new ArrayList<>();
        for(int number = pageStartNumber; number <= pageEndNumber; number++){
            manufacturers.addAll(getManufacturers(number));
        }
        List<Make> result = new ArrayList<>();
        for(Manufacturer manufacturer: manufacturers){
            result.addAll(getMakesForManufacturer(manufacturer));
        }
        return result;
    }

    @Override
    public List<Make> getAllMakes() {
        String websiteURL = "https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=json";
        String responseFromWebsite;

        Request request = new Request.Builder()
                .url(websiteURL)
                .build();

            //Executing the request as response
        try (Response response = httpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            //Printing Headers
            Headers responseHeaders = response.headers();
            for (int i = 0; i < responseHeaders.size(); i++) {
                System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
            }
                //Printing Response from Request
                responseFromWebsite =  response.body().string();
                JsonResultForMake makeResults = gsonParser.fromJson(responseFromWebsite, JsonResultForMake.class);
//                saveMakes(makeResults.results); //saves into Database
                return makeResults.results;
            } catch (Exception e) {
                System.out.println("Error occured during creating response: " + e.getMessage());
                return null;
            }
    }

    @Override
    public List<CarModel> getModelByMake(Make make) {
        //Taking ID from Make to search for CarModels
        int makeID = make.getId();
        //https://vpic.nhtsa.dot.gov/api/vehicles/GetModelsForMakeId/440?format=json

        try (Response response = queryForModel(makeID)) {
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response); //throw new - throws Exception
            String responseFromWebsite = response.body().string();
            List <CarModel> carModels = parseCarModels(responseFromWebsite);
            carModels.forEach(c -> c.setMake(make));
//            saveCarModels(carModels);
            return carModels;
        } catch (Exception e) {
            System.out.println("Error occured during creating response: " + e.getMessage());
            return null;
        }
    }

    private List<CarModel> parseCarModels (String responseFromWebsite) {
        JsonResultForCarModel carModels = gsonParser.fromJson(responseFromWebsite,JsonResultForCarModel.class);
        return carModels.results;
    }

    @Override
    public List<CarModel> getAllModels() {
        List<Make> makeList = getAllMakes();
        List<CarModel> result = new ArrayList<>();
        for(Make m: makeList) {
            result.addAll(getModelByMake(m));
        }
        return result;
    }

    private void saveManufacturers(List<Manufacturer> manufacturers) {
        OfflineVehicleAPI offlineAPI = new OfflineVehicleAPI();
        offlineAPI.saveManufacturers(manufacturers);
    }

    private void saveManufacturerDetails(Manufacturer manufacturer) {
        OfflineVehicleAPI offlineAPI = new OfflineVehicleAPI();
        offlineAPI.saveManufacturerDetails(manufacturer);
    }

    private void saveManufacturer(Manufacturer manufacturer) {
        OfflineVehicleAPI offlineAPI = new OfflineVehicleAPI();
        offlineAPI.saveManufacturer(manufacturer);
    }


    private void saveMakes(List<Make> makes) {
        OfflineVehicleAPI offlineAPI = new OfflineVehicleAPI();
        offlineAPI.saveMakes(makes);
    }
    private void saveCarModels(List<CarModel> carModels) {
        OfflineVehicleAPI offlineAPI = new OfflineVehicleAPI();
        offlineAPI.saveCarModels(carModels);
    }


    static class JsonResultForManufacturers {
        @SerializedName("Results")
        private List<Manufacturer> results;
        //Object containing results field
        JsonResultForManufacturers(List<Manufacturer> results) {
            this.results = results;
        }
    }

    static class JsonResultForManufacturerDetails {
        @SerializedName("Results")
        private List<ManufacturerDetails> results;
        JsonResultForManufacturerDetails(List <ManufacturerDetails> results) {
            this.results = results;
        }
    }

    static class JsonResultForMake {
        @SerializedName("Results")
        private List<Make> results;
        //Object containing results field
        JsonResultForMake(List<Make> results) {
            this.results = results;
        }
    }

    static class JsonResultForCarModel {
        @SerializedName("Results")
        private List<CarModel> results;
        //Object containing results field
        JsonResultForCarModel(List<CarModel> results) {
            this.results = results;
        }
    }

    private Response queryForModel(int number) throws IOException { //it may be exception, it is not throwing it;
        HttpUrl url = new HttpUrl.Builder()
                .scheme("https") //"https"
                .host("vpic.nhtsa.dot.gov")
                .addPathSegments("api/vehicles/GetModelsForMakeId")
                .addPathSegments(String.valueOf(number))
                .addQueryParameter("format", "json")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .build();
        return httpClient.newCall(request).execute();
    }

    private Response queryForManufacturers(int number) throws IOException { //it may be exception, it is not throwing it;
        HttpUrl url = new HttpUrl.Builder()
                .scheme("https") //"https"
                .host("vpic.nhtsa.dot.gov")
                .addPathSegments("api/vehicles/getallmanufacturers")
                .addQueryParameter("format", "json")
                .addQueryParameter("page",String.valueOf(number))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .build();
        return httpClient.newCall(request).execute();
    }

    private Response queryForManufacturerDetails(int number) throws IOException { //it may be exception, it is not throwing it;
        ///https://vpic.nhtsa.dot.gov/api/vehicles/getmanufacturerdetails/989?format=json
        HttpUrl url = new HttpUrl.Builder()
                .scheme("https") //"https"
                .host("vpic.nhtsa.dot.gov")
                .addPathSegments("api/vehicles/getmanufacturerdetails")
                .addPathSegment(String.valueOf(number))
                .addQueryParameter("format", "json")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .build();
        return httpClient.newCall(request).execute();
    }

    private Response queryForMake(int number) throws IOException { //it may be exception, it is not throwing it;
        //https://vpic.nhtsa.dot.gov/api/vehicles/GetMakeForManufacturer/988?format=json
        HttpUrl url = new HttpUrl.Builder()
                .scheme("https") //"https"
                .host("vpic.nhtsa.dot.gov")
                .addPathSegments("api/vehicles/GetMakeForManufacturer")
                .addPathSegments(String.valueOf(number))
                .addQueryParameter("format", "json")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .build();
        return httpClient.newCall(request).execute();
    }
}