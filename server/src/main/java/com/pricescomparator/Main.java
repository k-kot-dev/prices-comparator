package com.pricescomparator;

import com.pricescomparator.datamodel.CarModel;
import com.pricescomparator.datamodel.Make;
import com.pricescomparator.datamodel.Manufacturer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        int option = 0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(option >=0){
            consoleDisplay();
            option = chooseOption(reader);
            switch(option) {
                case 1: {
                    System.out.println("You have chosen: " + option);
                    dataTypeCommand();
                    int dataOption = chooseDataType(reader);
                    switch(dataOption) {
                        case 1: {
                            System.out.println("You have chosen OnlineAPI");
                            VehicleAPI dataSource = new OnlineVehicleAPI();
                            List<Manufacturer> manufacturers = dataSource.getAllManufacturers();
                            manufacturers.forEach(manufacturer -> System.out.println("Manufacturer ID: " + manufacturer.getId()
                                    + " " + "Manufacturer Name: " + manufacturer.getName()));
                            break;
                        }
                        case 2: {
                            System.out.println("You have chosen OfflineAPI");
                            VehicleAPI dataSource = new OfflineVehicleAPI();
                            List<Manufacturer> manufacturers = dataSource.getAllManufacturers();
                            manufacturers.forEach(manufacturer -> System.out.println("Manufacturer ID: " + manufacturer.getId()
                                    + " " + "Manufacturer Name: " + manufacturer.getName()));
                            break;
                        }
                        default: {
                            System.out.println("Please correct your number");
                        }
                    }
                    break;
                }
                case 2: {
                    System.out.println("You have chosen: " + option);
                    dataTypeCommand();
                    int dataOption = chooseDataType(reader);
                    switch(dataOption) {
                        case 1: {
                            System.out.println("You have chosen OnlineAPI");
                            System.out.println("\nPlease specify Maufacturer_ID");
                            int manufacturerID = chooseDataType(reader);
                            Manufacturer manufacturer = new Manufacturer();
                            manufacturer.setId(manufacturerID);
                            VehicleAPI dataSource = new OnlineVehicleAPI();
                            List<Make> makes = dataSource.getMakesForManufacturer(manufacturer);
                            makes.forEach(m -> System.out.println("Make ID: " + m.getId()
                                    + " " + "Make Name: " + m.getName()));
                            break;
                        }
                        case 2: {
                            System.out.println("You have chosen OfflineAPI");
                            System.out.println("\nPlease specify Maufacturer_ID");
                            int manufacturerID = chooseDataType(reader);
                            Manufacturer manufacturer = new Manufacturer();
                            manufacturer.setId(manufacturerID);
                            VehicleAPI dataSource = new OfflineVehicleAPI();
                            List<Make> makes = dataSource.getMakesForManufacturer(manufacturer);
                            makes.forEach(m -> System.out.println("Make ID: " + m.getId()
                                    + " " + "Make Name: " + m.getName()));
                            break;
                        }
                        default: {
                            System.out.println("Please correct your number");
                        }
                    }
                    break;
                }
                case 3: {
                    System.out.println("You have chosen: " + option);
                    dataTypeCommand();
                    int dataOption = chooseDataType(reader);
                    switch(dataOption) {
                        case 1: {
                            System.out.println("You have chosen OnlineAPI");
                            VehicleAPI dataSource = new OnlineVehicleAPI();
                            List<Make> makes = dataSource.getAllMakes();
                            makes.forEach(make -> System.out.println("Make ID: " + make.getId() + " "
                                    + "Make Name: " + make.getName()));
                            break;
                        }
                        case 2: {
                            System.out.println("You have chosen OfflineAPI");
                            VehicleAPI dataSource = new OfflineVehicleAPI();
                            List<Make> makes = dataSource.getAllMakes();
                            makes.forEach(make -> System.out.println("Make ID: " + make.getId() + " "
                                    + "Make Name: " + make.getName()));
                            break;
                        }
                        default: {
                            System.out.println("Please correct your number");
                        }
                    }
                    break;
                }
                case 4: {
                    System.out.println("You have chosen: " + option);
                    dataTypeCommand();
                    int dataOption = chooseDataType(reader);
                    switch(dataOption) {
                        case 1: {
                            System.out.println("You have chosen OnlineAPI");
                            System.out.println("\nPlease specify Make_ID");
                            int makeID = chooseDataType(reader);
                            Make m = new Make();
                            m.setId(makeID);
                            VehicleAPI dataSource = new OnlineVehicleAPI();
                            List<CarModel> carModels = dataSource.getModelByMake(m);
                            carModels.forEach(carModel -> System.out.println("CarModel ID: " + carModel.getId() + " "
                                    + "CarModel Name: " + carModel.getName()));
                            break;
                        }
                        case 2: {
                            System.out.println("You have chosen OfflineAPI");
                            System.out.println("\nPlease specify Make_ID");
                            int makeID = chooseDataType(reader);
                            Make m = new Make();
                            m.setId(makeID);
                            VehicleAPI dataSource = new OfflineVehicleAPI();
                            List<CarModel> carModels = dataSource.getModelByMake(m);
                            carModels.forEach(carModel -> System.out.println("CarModel ID: " + carModel.getId() + " "
                                    + "CarModel Name: " + carModel.getName()));
                            break;
                        }
                        default: {
                            System.out.println("Please correct your number");
                        }
                    }
                    break;
                }
                case 5: {
                    System.out.println("You have chosen: " + option);
                    dataTypeCommand();
                    int dataOption = chooseDataType(reader);
                    switch(dataOption) {
                        case 1: {
                            System.out.println("You have chosen OnlineAPI");
                            VehicleAPI dataSource = new OnlineVehicleAPI();
                            List<CarModel> carModels = dataSource.getAllModels();
                            carModels.forEach(carModel -> System.out.println("CarModel ID: " + carModel.getId() + " "
                                    + "CarModel Name: " + carModel.getName()));
                            break;
                        }
                        case 2: {
                            System.out.println("You have chosen OfflineAPI");
                            VehicleAPI dataSource = new OfflineVehicleAPI();
                            List<CarModel> carModels = dataSource.getAllModels();
                            carModels.forEach(carModel -> System.out.println("CarModel ID: " + carModel.getId() + " "
                                    + "CarModel Name: " + carModel.getName()));
                            break;
                        }
                        default: {
                            System.out.println("Please correct your number");
                        }
                    }
                    break;
                }
                case 0: {
                    System.out.println("Exiting program ...");
                    option=-1;
                    break;
                }
                default:
                    System.out.println("Please correct your number");
            }
        }

    }
    private static void consoleDisplay() {
        System.out.println("\nWhat do you want to do?");
        System.out.println("1.Read Manufacturers");
        System.out.println("2.Read Makes for specified Manufacturer");
        System.out.println("3.Read Makes");
        System.out.println("4.Read CarModels for specified Make");
        System.out.println("5.Read CarModels");
        System.out.println("0.Exit program\n");
    }

    private static void dataTypeCommand(){
        System.out.println("Do you want to retrieve data from Website(1) or from Database(2)?");
    }

    private static int chooseOption(BufferedReader reader) throws IOException {
        int option=-1; //-1 Here to close the application
        try{
            option = Integer.parseInt(reader.readLine());
            return option;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error occured during creating response: " + e.getMessage() + " Please choose once again");
            consoleDisplay(); //Ask once again after making error
            option = chooseOption(reader); //recursion for continuous errors
            return option;
        }
        return option;
    }

    private static int chooseDataType(BufferedReader reader) throws IOException {
        int option=-1; //-1 Here to close the application
        try{
            option = Integer.parseInt(reader.readLine());
            return option;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error occured during creating response: " + e.getMessage() + " Please choose once again");
            dataTypeCommand(); //Ask once again after making error
            option = chooseDataType(reader); //recursion for continuous errors
            return option;
        }
        return option;
    }
}
