package com.pricescomparator.security;

import com.pricescomparator.security.UserPrincipalDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity //It has @Configuration inside this annotation
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    //https://spring.io/blog/2015/06/08/cors-support-in-spring-framework
    //If you are using Spring Security, make sure to enable CORS at Spring Security level as well to allow it to leverage
    //the configuration defined at Spring MVC level.- this is made by .cors()

    // https://docs.spring.io/spring-security/site/docs/current/reference/html5/#mvc
    // Spring Framework provides first class support for CORS. CORS must be processed before Spring Security because the pre-flight
    // request will not contain any cookies (i.e. the JSESSIONID). If the request does not contain any cookies and Spring Security is
    // first, the request will determine the user is not authenticated (since there are no cookies in the request) and reject it.
    @Autowired
    private UserPrincipalDetailsService userPrincipalDetailsService;

    private final String ROLE_ADMIN = "ADMIN";
    private final String ROLE_USER = "USER";

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .cors() //You need it to be used by Front-End Application CORS - enables Spring CORSFILTER - otherwise 
                //Preflight Response is not working - approved by Spring documentation
                .and()
                .authorizeRequests()
                //It allows only for GET method
                .antMatchers("/login").authenticated()
                .antMatchers(HttpMethod.GET, "/makes/*/carmodels/**").permitAll() 
                .antMatchers("/makes/*/carmodels/**").authenticated() //Allows Post CarModel to Make

                .antMatchers(HttpMethod.GET, "/makes/**").permitAll() //Only admin can access this site
                .antMatchers("/makes/**").authenticated()

                .antMatchers(HttpMethod.GET, "/carmodels/**").permitAll()
                .antMatchers("/carmodels/**").authenticated()
                
                .antMatchers(HttpMethod.GET, "/manufacturers/**").permitAll()
                .antMatchers("/manufacturers/**").authenticated() //POST,PUT,DELETE

                .antMatchers(HttpMethod.GET, "/users").hasRole(ROLE_ADMIN)
                .antMatchers("/users").permitAll()

                .anyRequest().permitAll()
                .and()
                .httpBasic()
                // .and()
                // .formLogin()
                .and()
//                .logout()
//                .and()
                .csrf().disable(); 
                
                // CSRF().DISABLE() -> disable cross site request forgery, as we don't use cookies - otherwise ALL PUT, POST, DELETE will get HTTP 403!
                //You use it, when you want f.ex. to delete manufacturer - otherwise you get 403 
                // In order to use the Spring Security CSRF protection, we'll first need to make sure we use the proper HTTP methods for anything 
                // that modifies state (PATCH, POST, PUT, and DELETE – not GET).
                
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    //Database auth provider
    @Bean
    DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(userPrincipalDetailsService);
        return daoAuthenticationProvider;
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
