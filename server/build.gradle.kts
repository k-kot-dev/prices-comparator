plugins {
    id("org.springframework.boot") version "2.2.5.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    java
}

dependencies {
    implementation("com.google.code.gson:gson:2.8.2")
    implementation("com.googlecode.json-simple:json-simple:1.1.1")
    implementation("com.squareup.okhttp3:okhttp:3.14.4")
    implementation("org.hibernate:hibernate-core:5.4.11.Final")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-security")
    runtimeOnly("org.postgresql:postgresql")
}
