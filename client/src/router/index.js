import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home.vue'
import Contact from '@/components/Contact.vue'
import Make from '@/components/Make.vue'
import CarModel from '@/components/CarModel.vue'
import Manufacturer from '@/components/Manufacturer.vue'
import Login from '@/components/Login.vue'
import store from '../store'
import Register from '@/components/Register.vue'
import ResetPassword from '@/components/ResetPassword.vue'

Vue.use(Router) //It gives us access to {{$route.params}} - inside components

const router = new Router ({
    mode: 'history', //This way we remove # after localhost:8080/
    routes: [
    {
        path: '/',
        name: 'home',
        component : Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/contact',
        name: 'contact',
        component : Contact,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/makes',
        name: 'Makes',
        component : Make,
    },
    {
        path: '/carmodels',
        name: 'CarModels',
        component : CarModel,
    },
    {
        path: '/manufacturers',
        name: 'Manufacturers',
        component : Manufacturer,
    },
    {
        path: '/login',
        name: 'Login',
        component : Login
    },
    {
        path: '/register',
        name: 'Register',
        component : Register
    },
    {
        path: '/resetPassword',
        name: 'ResetPassword',
        component : ResetPassword
    },
    // otherwise redirect to home
    { path: '*', 
    redirect: '/' }
],

})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.getters.isLoggedIn) {
            next({
                path: '/login'
            })
        } else {
            next();
        }
    } else {
        next(); 
    }
});

export default router;