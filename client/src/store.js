import Vue from 'vue'
import Vuex from 'vuex'
import {AXIOS} from './components/http-commons'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loginSuccess: false,
        loginError: false,
        userName: null,
        passwordName: null,

        registerSuccess:false,
        registerError:false,
        userNameRegister: null,
        passwordNameRegister: null

    },
    //Mutators are used to change the state of a vuex store
    mutations: {
        login_success(state, payload){
            state.loginSuccess = true;
            state.userName = payload.userName;
            state.passwordName = payload.passwordName;

            console.log('User: ' + state.userName)
            console.log('Password: ' + state.passwordName)
        },
        login_error(state, payload){
            state.loginError = true;
            state.userName = payload.userName;
        },
        logout(state){
            state.loginSuccess = false;
            state.userName = null;
            state.passwordName = null;
        },

        register_success(state,payload){
            state.registerSuccess = true;
            state.userNameRegister = payload.userName;
            state.passwordNameRegister = payload.passwordName;

            console.log('User registered: ' + state.userNameRegister)
            console.log('Password registered: ' + state.passwordNameRegister)
        },
        register_error(state, payload){
            state.registerError = true;
            state.userNameRegister = payload.userName;
        },
    },

    actions: {
        login({commit}, {user, password}) {
            return new Promise((resolve, reject) => {
                console.log("Accessing backend with user: '" + user);
                //Here we check - if we have rights to log-in
                AXIOS.get("/login", { 
                    auth: {
                        username:  user,
                        password:  password  }})
                    .then(response => {
                        console.log('Statuscode: ' + response.status);
                        if(response.status == 200) {
                            console.log("Login successful");
                            // place the loginSuccess state into our vuex store
                            commit('login_success', {
                                userName: user,
                                passwordName: password
                            });
                        }
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error);
                        // place the loginError state into our vuex store
                        commit('login_error', {
                            userName: user
                        });
                        reject("Invalid credentials!")
                    })
            })
        },
        logout({commit}){
            console.log("Store.js Logout Operation")
            commit('logout')
        },
        register({commit}, {user, password}) {
            return new Promise((resolve, reject) => {
                console.log("Registering with user: " + user);
                //Here we check - if we have rights to log-in
                AXIOS.post("/users", { 
                  userName:  user,
                  password:  password  })
                    .then(response => {
                        console.log('Statuscode: ' + response.status);
                        if(response.status == 200) {
                            console.log("Register successful");
                            commit('register_success', {
                                userNameRegister: user,
                                passwordNameRegister: password
                            });
                        }
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error);
                        // place the loginError state into our vuex store
                        commit('register_error', {
                            userNameRegister: user
                        });
                        reject("User exists!")
                    })
            })
        },

    },

    getters: {
        isLoggedIn: state => state.loginSuccess,
        hasLoginErrored: state => state.loginError,
        getUserName: state => state.userName,
        getUserPass: state => state.passwordName,

        isRegistered: state => state.registerSuccess,
        hasRegistryErrored: state => state.registerError,
        getUserNameRegister: state => state.userNameRegister,
        getUserPassRegister: state => state.passwordNameRegister
    },

})
