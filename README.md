# Prices-comparator
Web application aimed to compare prices of the vehicles parts (source: NHTSA database). 

## Technologies used
Java 11, REST API, PostgreSQL, OkHTTP, GSON, Hibernate, Spring Boot, JavaScript, Vue.js, Gradle, GIT.

## Table of contents
* [Built with](#built-with)
* [Project status](#project-status)
* [Backend](#backend)
* [Frontend](#frontend)
* [Examples of use](#examples-of-use)
* [Author](#author)

## Built With

* [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/) - The backend framework used
* [Vue.js](https://devdocs.io/vue~1/) - The frontend framework used
* [Gradle](https://docs.gradle.org/current/userguide/userguide.html) - Dependency Management
* [Hibernate](https://hibernate.org/orm/documentation/5.4/) - ORM System - connected with PostgreSQL
 

 ## Project status
#### Next steps involve:
* Log-in for new users
* Adding Parts to database
* Cloud migration with AWS


## Backend
Data from NHTSA Database were parsed with library **OkHTTP**. Informations about Manufacturers, Makes and CarModels were saved into **PostgreSQL** database with use of **Hibernate**.

#### Relations between Data Models:
* Manufacturers - Makes - Many to many
* Makes - CarModels - One to Many

Next step was application of **Spring Boot** into project, to create **CRUD** operations for every Data Model.

#### Examples of REST API endpoints:
Read Makes (100 by Page)
```
GET /makes
```
Read Make
```
GET /makes/makeId
```
Create Make
```
POST /makes
```
Update Make
```
PUT /makes/makeId
```
Delete Make
```
DELETE /makes/makeId
```

#### Additionally Relations between f.ex. Makes and Manufacturers (Many to Many):

Read Manufacturers for Make
```
GET /makes/makeId/manufacturers
```
Add Manufacturer for Make
```
POST /makes/maksId/manufacturers/manufacturerId
```
Delete Manufacturer for Make
```
DELETE /makes/makeId
```
How problem with CORS (Cross-Origin Resource Sharing) was solved? By editing WebMvcConfigurer:
```
@Bean
public WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurerAdapter() {
        @Override
        public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
            .exposedHeaders("number-of-all-makes","number-of-all-carmodels","number-of-all-manufacturers");           
            }
        };
    }
```

## Frontend
Vue.js is used to present data and CRUD operations into website. 
#### Web App includes:
* data from REST API endpoints in tables
* routing
* pagination
* navigation bar (also for mobile phones)
* alerts - toasts
* bootstrap - CSS styling

## Examples of use
Every Model (Maufacturer, Make, CarModel) has it's own page - all CRUD operations are implemented from REST API into website:

![Logo](images/demoCarModel.png)

Every functionality is covered with toasts or alerts(in case of errors):
![Search](images/SearchCarModel.png)
![Error](images/ErrorCarModel.png)


Relations between f.ex CarModel and Make (Many to One):
![Search](images/CarModelsForMake.png)

Searching operations:
![Search](images/SearchMakeForCarModel.png)



## Author

* **Kacper Kot**



